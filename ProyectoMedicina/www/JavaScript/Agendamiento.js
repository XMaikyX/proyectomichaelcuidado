window.addEventListener('load',function(){
    btnnuevo.addEventListener('click',function(){
        txtnombre.value="";
        txtestado.value="";
        txtreceta.value="";
        txtcedula.value="";
        txthora.value="";
        txtdia.value="";
        txtdoctor.value="";
        txtmodulo.value="";
    })
    btnconsultar.addEventListener('click',function(){
        let url= `https://basedatosproyecto-77223.firebaseio.com/Citas.json`
        fetch(url).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{
            console.log(Object.entries(resultado2))
            let tabla= "<table border=1 class=contenido>";
            {
                tabla+="<tr>"
                tabla+=`<td>Estado</td> <td>Cedula</td> <td>Nombre</td> <td>Doctor</td> <td>Modulo</td> <td>Dia</td> <td>Hora</td> <td>Receta</td>`
                tabla+="</tr>" 
            }
            for(let elemento in resultado2)
            {
                tabla+="<tr>"
                tabla+=`<td> ${resultado2[elemento].estado}</td> <td> <button class='boton'> ${resultado2[elemento].ci}</button></td> <td> ${resultado2[elemento].nombre}</td> <td>${resultado2[elemento].doctor}</td> <td> ${resultado2[elemento].modulo}</td> <td>${resultado2[elemento].dia}</td> <td>${resultado2[elemento].hora}</td><td> ${resultado2[elemento].recetario}</td>`
                tabla+="</tr>"
            }
            tabla+= "</table>"
            datos.innerHTML=tabla

            document.querySelectorAll('.boton').forEach(elemento=>{
                elemento.addEventListener('click',function(){
                    let url1= `https://basedatosproyecto-77223.firebaseio.com/Citas/${elemento.innerHTML.trim()}.json`
                    console.log(url1)

                    fetch(url1).then(valores=>{return valores.json()}).then(valores2=>{
                        txtestado.value=valores2.estado;
                        txtcedula.value=valores2.ci;   
                        txtnombre.value=valores2.nombre;
                        txtdoctor.value=valores2.doctor;
                        txtmodulo.value=valores2.modulo;
                        txtdia.value=valores2.dia;
                        txthora.value=valores2.hora;
                        txtreceta.value=valores2.recetario;
                    } )
                })
            })
        })
        .catch(error=>{
            console.log(error)
        })
    })
    btngrabar.addEventListener('click',function(){
        let url= `https://basedatosproyecto-77223.firebaseio.com/Citas/${txtcedula.value}.json`
        let cuerpo={ci:txtcedula.value,nombre:txtnombre.value, estado: txtestado.value,dia:txtdia.value, hora: txthora.value,
        modulo:txtmodulo.value,doctor:txtdoctor.value, recetario:txtreceta.value};
        
        
        fetch(url,{
            method:'PUT',
            body: JSON.stringify(cuerpo),
            headers:{
                'Content-Type':'application/json'
            }
        }).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{
            alert("se ha Registrado")
            console.log(resultado2.nombre)
        })
        .catch(error=>{
            console.log('No se ha añadido',error)
        })
    })
    btneliminar.addEventListener('click',function(){
        let url= `https://basedatosproyecto-77223.firebaseio.com/Citas/${txtcedula.value}.json`    
        
        fetch(url,{
            method:'DELETE'
        }).then(resultado=>{
            alert("se ha Eliminado correctamente")
            return resultado.json();
        })
        .then(resultado2=>{
            
            console.log(resultado2.nombre)
        })
        .catch(error=>{
            console.log('No se ha Eliminado',error)
        })
    })

})