window.addEventListener('load',function(){
    //DOCTORES
    btnnuevo.addEventListener('click',function(){
        txtcodigo.value="";
        txtnombre.value="";
    })
    btnconsultar.addEventListener('click',function(){
        let url= `https://basedatosproyecto-77223.firebaseio.com/Doctores.json`
        fetch(url).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{
            console.log(Object.entries(resultado2))
            let tabla= "<table border=1 class=contenido1>";
            {
                tabla+="<tr>"
                tabla+=`<td>Codigo</td> <td>Nombre</td> `
                tabla+="</tr>" 
            }
            for(let elemento in resultado2)
            {
                tabla+="<tr>"
                tabla+=`<td> <button class='boton'>${resultado2[elemento].codigo}</button></td> <td>  ${resultado2[elemento].nombre}</td>`
                tabla+="</tr>"
            }
            tabla+= "</table>"
            datos.innerHTML=tabla

            document.querySelectorAll('.boton').forEach(elemento=>{
                elemento.addEventListener('click',function(){
                    let url1= `https://basedatosproyecto-77223.firebaseio.com/Doctores/${elemento.innerHTML.trim()}.json`
                    console.log(url1)

                    fetch(url1).then(valores=>{return valores.json()}).then(valores1=>{
                        txtcodigo.value=valores1.codigo;
                        txtnombre.value=valores1.nombre;   
                    } )
                })
            })
        })
        .catch(error=>{
            console.log(error)
        })
    })
    btngrabar.addEventListener('click',function(){
        let url= `https://basedatosproyecto-77223.firebaseio.com/Doctores/${txtcodigo.value}.json`
        let cuerpo={codigo:txtcodigo.value,nombre:txtnombre.value};
        
        
        fetch(url,{
            method:'PUT',
            body: JSON.stringify(cuerpo),
            headers:{
                'Content-Type':'application/json'
            }
        }).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{
            alert("se ha Registrado Doctor")
            console.log(resultado2.nombre)
        })
        .catch(error=>{
            console.log('No se ha añadido',error)
        })
    })
    btneliminar.addEventListener('click',function(){
        let url= `https://basedatosproyecto-77223.firebaseio.com/Doctores/${txtcodigo.value}.json`    
        
        fetch(url,{
            method:'DELETE'
        }).then(resultado=>{
            alert("se ha Eliminado el Doctor correctamente")
            return resultado.json();
        })
        .then(resultado2=>{
            
            console.log(resultado2.nombre)
        })
        .catch(error=>{
            console.log('No se ha Eliminado',error)
        })
    })

    //MODULOS DE MEDICINA

    btnnuevo1.addEventListener('click',function(){
        txtcodigo1.value="";
        txtnombre1.value="";
    })
    btnconsultar1.addEventListener('click',function(){
        let url= `https://basedatosproyecto-77223.firebaseio.com/Modulo.json`
        fetch(url).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{
            console.log(Object.entries(resultado2))
            let tabla= "<table border=1 class=contenido2>";
            {
                tabla+="<tr>"
                tabla+=`<td>Codigo</td> <td>Nombre</td> `
                tabla+="</tr>" 
            }
            for(let elemento in resultado2)
            {
                tabla+="<tr>"
                tabla+=`<td> <button class='boton'>${resultado2[elemento].codigo}</button></td> <td>  ${resultado2[elemento].nombre}</td>`
                tabla+="</tr>"
            }
            tabla+= "</table>"
            datos1.innerHTML=tabla

            document.querySelectorAll('.boton').forEach(elemento=>{
                elemento.addEventListener('click',function(){
                    let url1= `https://basedatosproyecto-77223.firebaseio.com/Modulo/${elemento.innerHTML.trim()}.json`
                    console.log(url1)

                    fetch(url1).then(valores=>{return valores.json()}).then(valores1=>{
                        txtcodigo1.value=valores1.codigo;
                        txtnombre1.value=valores1.nombre;   
                    } )
                })
            })
        })
        .catch(error=>{
            console.log(error)
        })
    })
    btngrabar1.addEventListener('click',function(){
        let url= `https://basedatosproyecto-77223.firebaseio.com/Modulo/${txtcodigo1.value}.json`
        let cuerpo={codigo:txtcodigo1.value,nombre:txtnombre1.value};
        
        
        fetch(url,{
            method:'PUT',
            body: JSON.stringify(cuerpo),
            headers:{
                'Content-Type':'application/json'
            }
        }).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{
            alert("se ha Registrado Modulo")
            console.log(resultado2.nombre)
        })
        .catch(error=>{
            console.log('No se ha añadido Modulo',error)
        })
    })
    btneliminar1.addEventListener('click',function(){
        let url= `https://basedatosproyecto-77223.firebaseio.com/Modulo/${txtcodigo1.value}.json`    
        
        fetch(url,{
            method:'DELETE'
        }).then(resultado=>{
            alert("se ha Eliminado el Modulo correctamente")
            return resultado.json();
        })
        .then(resultado2=>{
            
            console.log(resultado2.nombre)
        })
        .catch(error=>{
            console.log('No se ha Eliminado',error)
        })
    })

})