window.addEventListener('load',function(){
    btnnuevo.addEventListener('click',function(){
        txtcodigo.value="";
        txtdescripcion.value="";
    })
    btnconsultar.addEventListener('click',function(){
        let url= `https://basecomplementario.firebaseio.com/Usuarios.json`
        fetch(url).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{
            console.log(Object.entries(resultado2))
            let tabla= "<table border=1>";
            {
                tabla+="<tr>"
                tabla+=`<td>Nombre</td> <td>Apellido</td> <td>Correo</td> <td>Contraseña</td>`
                tabla+="</tr>" 
            }
            for(let elemento in resultado2)
            {
                tabla+="<tr>"
                tabla+=`<td> ${resultado2[elemento].Nombre}</td> <td>${resultado2[elemento].Apellido}</td> <td>${resultado2[elemento].Correo}</td> <td>${resultado2[elemento].Contraseña}</td> <td> <button class="eliminar">Eliminar</button></td>`
                tabla+="</tr>"
            }
            tabla+= "</table>"
            datos.innerHTML=tabla
        })
        .catch(error=>{
            console.log(error)
        })
    })
    btngrabar.addEventListener('click',function(){
        //POST
        //PUT
        let url= `https://basecomplementario.firebaseio.com/Usuarios/${txtnombre.value}.json`
        let cuerpo={Nombre: txtnombre.value, Apellido: txtapellido.value, Correo: txtcorreo.value, Contraseña: txtcontraseña.value, CRepetida: txtconfirmar.value}
        
        
        fetch(url,{
            method:'PUT',
            body: JSON.stringify(cuerpo),
            headers:{
                'Content-Type':'application/json'
            }
        }).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{
            alert(resultado2.Nombre+"se ha Registrado")
            console.log(resultado2.Nombre)
        })
        .catch(error=>{
            console.log('No se ha añadido',error)
        })
    })
    btneliminar.addEventListener('click',function(){
        let url= `https://basecomplementario.firebaseio.com/Usuarios/${txtnombre.value}.json`    
        
        fetch(url,{
            method:'DELETE'
        }).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{
            alert(resultado2.Nombre+"se ha Eliminado")
            console.log(resultado2.Nombre)
        })
        .catch(error=>{
            console.log('No se ha Eliminado',error)
        })
    })

})