import { Component, OnInit } from '@angular/core';
import { Cita } from 'src/app/interfaces/cita';
import { ICrud } from 'src/app/interfaces/ICrud';
import { CittassService } from 'src/app/services/cittass.service';

@Component({
  selector: 'app-cita',
  templateUrl: './cita.page.html',
  styleUrls: ['./cita.page.scss'],
})
export class CitaPage implements OnInit, ICrud {

  public citaMedica: Cita = {ci:'000', doctor:'Vacio', dia:0, descripcion:'vacio'}

  public citasMedica: Cita[]=
  [
    //{ci:'1251354848', doctor:'Manolo', dia:15, descripcion:'dermatologia'},
    //{ci:'1254687568', doctor:'Jose', dia:20, descripcion:'medicina general'}
  ];


  constructor(private cliente:CittassService) { 
    
  }
  
  guardar(): void {
    this.cliente.postCita(this.citaMedica).then(resp=>{
      console.log('se grabo su cita')
    })
    .catch(error=>
      console.log('error al grabar su cita')
      )
  }

  consultar(): void {
    this.cliente.getCita().then(respuesta=>{
      
      this.citasMedica=[];
      for(let elemento in respuesta)
      {
        this.citasMedica.push(respuesta[elemento]);
      }
      
    })
    .catch(error=>{
      console.log(error)
    })
  }
  eliminar(): void {
    this.cliente.deleteCita(this.citaMedica.ci).then(resp=>{
      console.log('se elimino correctamente')  
    })
    .catch(error=>{
      console.log('no se pudo eliminar')
    })
  }

  ngOnInit() {
  }

  nuevo()
  {
    this.citaMedica={ci:'',dia:0,doctor:"",descripcion:""};
  }



}
