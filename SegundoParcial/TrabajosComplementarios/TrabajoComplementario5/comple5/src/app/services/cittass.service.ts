import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cita } from  '../interfaces/cita'

@Injectable({
  providedIn: 'root'
})
export class CittassService {

  apiURL='https://complementario5.firebaseio.com/Citas'

  constructor(private clienteServicio:HttpClient) { }
  
  //condultar las citas generadas
  public getCita(ced='')
  {
    if (ced=='')
    return this.clienteServicio.get(`${this.apiURL}.json`).toPromise()
    return this.clienteServicio.get(`${this.apiURL}/${ced}.json`).toPromise()
  }
  
  
  //crear-Modificar
  public postCita( citax:Cita)
  {
    return this.clienteServicio.put(`${this.apiURL}/${citax.ci}.json`
    ,citax, {headers :{'Content-Type':'application/json'}} ).toPromise();
  }
  
  //eliminar
  public deleteCita(ceds:string)
  {
    return this.clienteServicio.delete(`${this.apiURL}/${ceds}.json`).toPromise();
  }
}
