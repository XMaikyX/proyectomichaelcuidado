window.addEventListener('load', function(){
    btnconsultar.addEventListener('clic', function(){
        httpGet("./https://jsonplaceholder.typicode.com/posts")
        .then(JSON.parse)
        .then(jsonresponse=> jsonresponse[0])
        .then(console.log)
        .catch(error=> console.log(error))
    })
})

function httpGet(url)
{
    const xhr= XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onload = ()=>{
        resolver(xhr.responsetext)
    };
    xhr.onerror= ()=>{
        reject (xhr.statusText);
    };
    xhr.send();
}